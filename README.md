# tju_undergraduate_design

#### 项目介绍
天津大学本科毕业论文LATEX模板


#### 主要内容
主要包括两部分内容：
- 毕业论文/毕业设计说明书的模板，位于thesis文件夹下
- 任务书的模板，位于assignment book文件夹下



#### 环境配置

支持CTex和Texlive，推荐使用 VS Code + Latex Workshop + Texlive进行撰写。

#### 使用说明

1. 任务书模板
tjumain.tex             主模板文件，作者需要在该文件下完善封面信息
body/background.tex     在此文件内撰写任务书“工作背景”部分的内容
body/reference.bib      在此文件中撰写任务书“参考文献”部分的内容，使用bibtex格式
body/requirements.tex   在此文件内撰写任务书“内容与要求”部分的内容
figure/*                任务书中所使用的图片
setup/*                 格式设置文件


2. 毕业论文模板
tjumain.tex                     主模板文件
body/                           用户在此文件夹下编写论文各章节内容
figure/*                        论文中中所使用的图片
setup/info.tex                  论文基本信息、封面信息、摘要关键字信息设置。需要在此文件中切换论文/设计任务书模板【重要】
references/reference.bib        参考文献
appendix/paperInEnglish.tex     外文资料索引页
appendix/paperInChinese.tex     中文译文
appendix/acknowledgements.tex   致谢

#### 如何参与贡献

1. Fork 本项目
2. 新建分支
3. 提交代码
4. 创建 Pull Request